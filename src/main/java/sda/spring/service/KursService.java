package sda.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sda.spring.model.Kurs;

import sda.spring.model.User;
import sda.spring.repository.KursRepository;
import sda.spring.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class KursService {
    @Autowired
    private KursRepository kursRepository;
    @Autowired
    private UserRepository userRepository;

    @Transactional
    public void registerKurs(Kurs kurs) {
        this.kursRepository.save(kurs);
    }
    @Transactional
    public void editKurs(Kurs zmieniony) {
        Kurs kurs = this.kursRepository.findByName(zmieniony.getName());
        kurs.setDataDo(zmieniony.getDataDo());
        kurs.setDataOd(zmieniony.getDataOd());
        kurs.setUserList(zmieniony.getUserList());
        kursRepository.save(kurs);
    }
    public boolean userToKurs(String kursName,String username) {
        if(kursPresent(kursName)) {
            Kurs kurs = this.kursRepository.findByName(kursName);
            kurs.getUserList().add(userRepository.findByUsername(username));
            kursRepository.save(kurs);
            return true;
        }
        return false;
    }

    public void userDropKurs( String kursName,User user) {
        Kurs kurs = this.kursRepository.findByName(kursName);
        kurs.getUserList().remove(user);
        kursRepository.save(kurs);
    }

    public List<User> getUsersFromKurs(String name) {

        if(kursPresent(name)){
            Kurs kurs = this.kursRepository.findByName(name);
            return kurs.getUserList();
        }
        return null;
    }

    public Kurs getKurs(String kursName) {
        if(kursPresent(kursName)){
        Kurs kurs = this.kursRepository.findByName(kursName);
        return kurs;
        }
        return null;
    }

    public boolean kursPresent(String kursName) {
        if (!this.kursRepository.findByName(kursName).equals(null)) {
            return true;
        }
        return false;
    }
}
