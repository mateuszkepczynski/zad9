package sda.spring.service;

import org.springframework.stereotype.Service;
import sda.spring.model.User;
import sda.spring.model.UserDto;

@Service
public class UserMap {
    public User map(UserDto user) {
        User entity = new User();
        entity.setAddress(user.getAddress());
        entity.setEmail(user.getEmail());
        entity.setFirstname(user.getFirstname());
        entity.setLastname(user.getLastname());
        entity.setPassword(user.getPassword());
        entity.setPhone(user.getPhone());
        entity.setUsername(user.getUsername());
        return entity;
    }
}
