package sda.spring.service;

import org.springframework.stereotype.Service;
import sda.spring.model.Kurs;
import sda.spring.model.KursDto;

@Service
public class KursMap {

    public Kurs map(KursDto kurs) {
        Kurs entity = new Kurs();

        entity.setUserList(kurs.getUserList());

        entity.setDataDo(kurs.getDataDo());
        entity.setDataOd(kurs.getDataOd());
        entity.setName(kurs.getName());
        return entity;
    }
}
