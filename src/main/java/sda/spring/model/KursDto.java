package sda.spring.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@ApiModel(description = "Kurs")
public class KursDto {

    private List<User> userList = new ArrayList<>();
    @ApiModelProperty(value = "Nazwa")
    @JsonProperty("name")
    private String name;
    @ApiModelProperty(value = "Data od")
    @JsonProperty("dataOd")
    private Date dataOd;
    @ApiModelProperty(value = "Data do")
    @JsonProperty("dataDo")
    private Date dataDo;

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDataOd() {
        return dataOd;
    }

    public void setDataOd(Date dataOd) {
        this.dataOd = dataOd;
    }

    public Date getDataDo() {
        return dataDo;
    }

    public void setDataDo(Date dataDo) {
        this.dataDo = dataDo;
    }
}
