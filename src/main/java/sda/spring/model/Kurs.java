package sda.spring.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Kurs {
    @Id
    @GeneratedValue
    private Long id;
    @OneToMany
    private List<User> userList = new ArrayList<>();
    private String name;
    @ApiModelProperty(value = "Data od", dataType="java.util.Date")
    @JsonProperty("dataOd")
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dataOd;
    @ApiModelProperty(value = "Data od", dataType="java.util.Date")
    @JsonProperty("dataDo")
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dataDo;


    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDataOd() {
        return dataOd;
    }

    public void setDataOd(Date dataOd) {
        this.dataOd = dataOd;
    }

    public Date getDataDo() {
        return dataDo;
    }

    public void setDataDo(Date dataDo) {
        this.dataDo = dataDo;
    }

    public Long getId() {
        return id;
    }
}
