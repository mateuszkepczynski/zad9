package sda.spring.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import sda.spring.model.KursDto;

import sda.spring.service.KursMap;
import sda.spring.service.KursService;


@RestController
public class KursController {

    @Autowired
    KursService kursService;
    @Autowired
    KursMap kursMap;


    @RequestMapping(value = "/registeredKurs", method = RequestMethod.POST, consumes= MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Dodaje kurs.", notes = "", response = Void.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Kurs dodany", response = Void.class),
            @ApiResponse(code = 401, message = "Problem z dodaniem kursu", response = Void.class) })
    public ResponseEntity<Void> registeredProcess(@ApiParam(value = "Dane kursu") @RequestBody KursDto kurs) {
        kursService.registerKurs(kursMap.map(kurs));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/editedKurs", method = RequestMethod.POST, consumes= MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Edytuje kurs.", notes = "", response = Void.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Kurs zedytowany", response = Void.class),
            @ApiResponse(code = 401, message = "Problem z edycja kursu", response = Void.class) })
    public ResponseEntity<Void> editedProcess(@ApiParam(value = "Nowe dane kursu") @RequestBody KursDto kurs) {
        kursService.editKurs(kursMap.map(kurs));
            return new ResponseEntity<>(HttpStatus.OK);

    }

    @RequestMapping(value = "/userToKurs", method = RequestMethod.PUT, consumes= MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Dodaje usera do kursu.", notes = "", response = Void.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "User został dodany do kursu", response = Void.class),
            @ApiResponse(code = 401, message = "Problem z dodaniem usera do kursu", response = Void.class) })
    public ResponseEntity<Void> addUserToKursProcess(@ApiParam(value = "Dane nazwa kursu i usera") @RequestParam ("Kurs") String kursName, @RequestParam ("User") String userName) {
        if(kursService.userToKurs(kursName,userName)){
        return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(value = "/getUsersFromKurs", method = RequestMethod.POST, consumes= MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Wypisuje user'ow z kursu.", notes = "", response = Void.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Userzy wypisani", response = Void.class),
            @ApiResponse(code = 401, message = "Problem z wypisanie usera z kursu", response = Void.class) })
    public ResponseEntity<Void> getUsersFromKurs(@ApiParam(value = "Dane nazwa kursu i usera") @RequestParam ("Kurs") String kursName) {
        if(!kursService.getUsersFromKurs(kursName).isEmpty()){
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

    }





}
