package sda.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sda.spring.model.Kurs;


public interface KursRepository extends JpaRepository<Kurs, Long> {

    Kurs findByName(String name);
}
